from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import *
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.

class uniTests(TestCase):

    def test_ada_url(self):
        response = Client().get("")
        self.assertEqual(response.status_code,200)

    def test_ada_fungsi(self):
        response = resolve("/")
        self.assertEqual(response.func,index)

    def test_ada_template(self):
        response = Client().get("")
        self.assertTemplateUsed(response,"index.html")

    def test_ada_button(self):
        response = Client().get("")
        content = response.content.decode("utf8")
        self.assertIn("</button",content)

class functionalTests(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)

    def tearDown(self):
        self.browser.quit()

    def test_masukin_form(self):
        self.browser.get("http://127.0.0.1:8000/")
        time.sleep(5)

        search_box = self.browser.find_element_by_id("searchbox")
        search_box.send_keys("Overlord")
        search_button = self.browser.find_element_by_name("masuk")
        search_button.click()

        time.sleep(3)
        self.assertIn("Overlord", self.browser.page_source)
