
var input = document.getElementById("searchbox");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Trigger the button element with a click
        document.getElementById("submit").click();
    }
});

function loadScreen(){
  var link = 'https://www.googleapis.com/books/v1/volumes?q=python';
  var query = document.getElementById("searchbox").value;
  console.log(query);

  var table = '<tr><th id = "number">No.</th><th id = "title">Title</th><th id = "author">Author</th><th id = "publisher">Publisher</th><th id = "year">Year</th></tr>';
  var noinfo = '<td style = "color: #bbb">No info</td>';
  var counter = 1;

  console.log(link);

  $.ajax({
      url: link,
      success: function(data){
          $('#book_container').empty();

          $.each(data.items, function(i, item){
              table += '<tr>';
              table += '<td>' + counter + '</td>';
              table += '<td>' + item.volumeInfo.title + '</td>';

              if (!item.volumeInfo.authors) {
                  table += noinfo;
              }
              else {
                  table += '<td>'
                  var authors = ''
                  authors += item.volumeInfo.authors;
                  var author = authors.split(',');
                  for (i = 0; i < author.length; i++) {
                      table += author[i];
                      if (i < author.length - 1) {
                          table += ', '
                      }
                  }
                  table += '</td>';
              }

              if (!item.volumeInfo.publisher) {
                  table += noinfo;
              }
              else {
                  table += '<td>' + item.volumeInfo.publisher + '</td>';
              }

              if (!item.volumeInfo.publishedDate) {
                  table += noinfo;
              }
              else {
                  table += '<td>' + item.volumeInfo.publishedDate + '</td>';
              }
              table += '</tr>';
              counter++;
          })

          $('#book_container').append(table);
      }})}
}

function search(){
    var link = 'https://www.googleapis.com/books/v1/volumes?q=';
    var query = document.getElementById("searchbox").value;
    console.log(query);
    var words = query.split(' ');
    for (i = 0; i < words.length; i++) {
        link += words[i];
        if (i < words.length-1) {
            link += '+';
        }
    }

    var table = '<tr><th id = "number">No.</th><th id = "title">Title</th><th id = "author">Author</th><th id = "publisher">Publisher</th><th id = "year">Year</th></tr>';
    var noinfo = '<td style = "color: #bbb">No info</td>';
    var counter = 1;

    console.log(link);

    $.ajax({
        url: link,
        success: function(data){
            $('#book_container').empty();

            $.each(data.items, function(i, item){
                table += '<tr>';
                table += '<td>' + counter + '</td>';
                table += '<td>' + item.volumeInfo.title + '</td>';

                if (!item.volumeInfo.authors) {
                    table += noinfo;
                }
                else {
                    table += '<td>'
                    var authors = ''
                    authors += item.volumeInfo.authors;
                    var author = authors.split(',');
                    for (i = 0; i < author.length; i++) {
                        table += author[i];
                        if (i < author.length - 1) {
                            table += ', '
                        }
                    }
                    table += '</td>';
                }

                if (!item.volumeInfo.publisher) {
                    table += noinfo;
                }
                else {
                    table += '<td>' + item.volumeInfo.publisher + '</td>';
                }

                if (!item.volumeInfo.publishedDate) {
                    table += noinfo;
                }
                else {
                    table += '<td>' + item.volumeInfo.publishedDate + '</td>';
                }
                table += '</tr>';
                counter++;
            })

            $('#book_container').append(table);
        }})}
